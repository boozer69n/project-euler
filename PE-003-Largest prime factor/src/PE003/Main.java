package PE003;
/**
 * Largest prime factor
 * Problem 3
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 *
 */
public class Main {
	public static void main(String[] args) {
		int x = 2;
		long y = 600851475143L;
		long check;		
		while (x < y) {
			check = y;
			if (y % x == 0) {
				y /= x;
			if (check == y) {
				x++;
			}
			} else {
				x++;
			}
		}
		System.out.println(y);
	}
}
