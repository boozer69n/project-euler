package PE009;
/**
 * Special Pythagorean triplet
 * Problem 9
 * Published on Friday, 25th January 2002, 11:00 am; Solved by 201660; Difficulty rating: 5%
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 *
 *a^2 + b^2 = c^2
 *
 *For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 *
 *There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 *Find the product abc.
 */

public class Main {

	public static void main(String[] args) {
		int a = 0;
		int b = 1;
		int c = 2;
		while (a < 1000 && !check(pyth(a,b,c), equal(a,b,c))) {
			a++;
			b = a;
			while (b < 1000 && !check(pyth(a,b,c), equal(a,b,c))) {
				b++;
				c = b;
				while (c < 1000 && !check(pyth(a,b,c), equal(a,b,c))) {
					c++;
				}
			}
		}
		System.out.printf("A: %d, B: %d, C: %d%n", a, b, c);
		System.out.printf("Product: %d", a*b*c);
	}
	
	public static boolean check(boolean py, boolean eq) {
		if (py && eq) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean pyth(int x, int y, int z) {
		return (x*x + y*y) == z*z;
	}
	
	public static boolean equal(int x, int y, int z) {
		return (x + y + z) == 1000;
	}
}