package PE004;
/**
 * Largest palindrome product
 * Problem 4
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 � 99.
 * 
 * Find the largest palindrome made from the product of two 3-digit numbers.
 *
 */
public class Main {
	public static void main(String[] args) {
		int x = 100;
		int y = 100;
		int product = 0;
		int high = 0;
		String original = "";
		String reverse = "";
		
		while (x < 1000) {
			y = 100;
			while (y < 1000) {
				product = x * y;
				original = String.valueOf(product);
				for (int i = original.length() - 1; i >=0; i--) {
					reverse = reverse + original.charAt(i);
				}
				if (original.equals(reverse)) {
					if (product > high) {
						high = product;
					}
					reverse = "";
				} else {
					reverse = "";
				}
			y++;
			}
		x++;
		}
		System.out.println(high);
	}
}
