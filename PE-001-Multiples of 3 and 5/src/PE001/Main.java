package PE001;
/**
 * Multiples of 3 and 5
 * Problem 1
 * Published on Friday, 5th October 2001, 10:00 am; Solved by 478007; Difficulty rating: 5%
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * 
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
public class Main {
	public static void main(String[] args) {
		int x = 1;
		int sum_total = 0;
		while (x < 1000) {
			if (x % 3 == 0 || x % 5 == 0){
				sum_total += x;
			}
			x++;
		}
		System.out.println(sum_total);
	}
}
