package PE005;
/**
 * Smallest multiple
 * Problem 5
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * 
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 *
 */
public class Main {
	public static void main(String[] args) {
		int start = 20;
		int check;
		boolean shouldExit = false;
		
		while (!shouldExit) {
			shouldExit = true;
			check = 0;
			for (int i = 2; i <= 20; i++) {
				if (start % i == 0) {
					if (check == start - 1) {
						shouldExit = true;
					}
				} else {
					i = 21;
					start += 20;
					shouldExit = false;
				}
			}
		}
		System.out.println(start);
	}
}