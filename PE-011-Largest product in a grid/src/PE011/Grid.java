package PE011;

public class Grid {
	
	/**
	 * creates a 2d array from a 1d array and the x/y sizes (no error checking)
	 * @param x: number of horizontal ints
	 * @param y: number of vertical ints
	 * @param a: the array
	 * @return: 2d array
	 */
	public int[][] make_grid(int x, int y, int[] a){
		int grid[][] = new int[x-1][y-1];
		int hor = 0;
		int ver = 0;
		int loc = 0;
		while (hor < x){
			while (ver < y){
				grid[hor][ver] = a[loc];
				loc++;
			}
		}
		return grid;
	}	
}