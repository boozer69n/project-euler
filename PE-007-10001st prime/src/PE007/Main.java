package PE007;
/**
 * 10001st prime
 * Problem 7
 * Published on Friday, 28th December 2001, 11:00 am; Solved by 232727; Difficulty rating: 5%
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * 
 * What is the 10,001st prime number?
 */
public class Main {
	public static void main(String[] args) {
		PE007_Primes p = new PE007_Primes();
		System.out.println(p.prime_by_location(1, 10001));
	}
}