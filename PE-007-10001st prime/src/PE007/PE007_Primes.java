package PE007;

public class PE007_Primes {
	
	/**
	 * @param start: the number you want to start searching for prime numbers
	 * @param location: the number of prime numbers from the start parameter
	 * @return: the prime number at the location
	 * example: 2, 3, 5, 7 - the 7 is the 4th prime(location)
	 */
	public int prime_by_location(int start, int location){
		int count = 0;
		int current = start;
		while (count != location) {
			if (isPrime(current)) {
				count++;
			}
			current++;
		}
		return current-1;
	}
	
	/**
	 * returns a boolean on whether or not the number is prime
	 * @param number: the number you want to check
	 * @return: boolean
	 */
	public boolean isPrime(int number) {
		if (number <= 1) { return false; }
		if (number <= 3) { return true; }		
		int limit = number / 2;
		int check = 2;
		while (check <= limit) {
			if (number % check == 0) {
				return false;
			}
			check++;
		}
		return true;		
	}
	
	/**
	 * returns a boolean on whether or not the number is prime
	 * @param number: the number you want to check
	 * @return: boolean
	 */
	public boolean isPrime(long number) {
		if (number <= 1) { return false; }
		if (number <= 3) { return true; }		
		long limit = number / 2;
		long check = 2;
		while (check <= limit) {
			if (number % check == 0) {
				return false;
			}
			check++;
		}
		return true;		
	}
}