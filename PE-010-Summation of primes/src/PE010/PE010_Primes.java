package PE010;

import PE007.*;

public class PE010_Primes {
	PE007_Primes p = new PE007_Primes();
	
	public long prime_add(long start, long end) {
		long begin = start;
		long total = 0;
		
		while(begin <= end){
			if(p.isPrime(begin)){
				total += begin;
			}
			begin++;
		}
		return total;		
	}	
}