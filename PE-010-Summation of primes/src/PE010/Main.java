package PE010;
/**
 *Summation of primes
 *Problem 10
 *The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 *Find the sum of all the primes below two million.
 */


public class Main {

	public static void main(String[] args) {
		PE010_Primes p = new PE010_Primes();
		System.out.println(p.prime_add(0, 2000000));		
	}
}